﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using TP3P1.Models.EntityFramework;

namespace TP3P1.Models.EntityFramework
{
    [Table("T_E_FILM_FLM")]
    public class Film
    {
        public Film()
        {
            FavorisFilm = new HashSet<Favori>();
        }

        //[Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("FLM_ID")]
        public int FilmId { get; set; }
        [Required]
        [Column("FLM_TITRE")]
        [StringLength(100)]
        public string Titre { get; set; }
        [Column("FLM_SYNOPSIS")]
        [StringLength(500)]
        public string Synopsis { get; set; }
        [Required]
        [Column("FLM_DATEPARUTION", TypeName = "date")]
        public DateTime DateParution { get; set; }
        [Column("FLM_DUREE", TypeName = "numeric(3, 0)")]
        public decimal Duree { get; set; }
        [Required]
        [Column("FLM_GENRE")]
        [StringLength(30)]
        public string Genre { get; set; }
        [Column("FLM_URLPHOTO")]
        [StringLength(200)]
        public string UrlPhoto { get; set; }

        [InverseProperty("FilmFavori")]
        public ICollection<Favori> FavorisFilm { get; set; }
    }
}
