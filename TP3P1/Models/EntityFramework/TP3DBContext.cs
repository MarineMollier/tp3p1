﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TP3P1.Models.EntityFramework;

namespace TP3P1.Models.EntityFramework
{
    public class FilmRatingsDBContext : DbContext
    {
        public FilmRatingsDBContext()
        {
        }

        public FilmRatingsDBContext(DbContextOptions<FilmRatingsDBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Compte> Compte { get; set; }
        public virtual DbSet<Film> Film { get; set; }
        public virtual DbSet<Favori> Favori { get; set; }

        /*protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseNpgsql("Server=localhost;port=5432;Database=FilmRatingsDB; uid=postgres; password=postgres;");
            }
        }*/

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("public");

            modelBuilder.Entity<Compte>(entity =>
            {
                entity.HasKey(e => new { e.CompteId }).HasName("PK_CPT");
                entity.HasIndex(e => e.Mel)
                    .HasName("UQ_CPT_MEL")
                    .IsUnique();
                entity.Property(e => e.Pays).HasDefaultValue("France");
                entity.Property(e => e.DateCreation).HasDefaultValueSql("current_date");
            });

            modelBuilder.Entity<Film>(entity =>
            {
                entity.HasKey(e => new { e.FilmId }).HasName("PK_FLM");
            });

            modelBuilder.Entity<Favori>(entity =>
            {
                entity.HasKey(e => new { e.CompteId, e.FilmId }).HasName("PK_FAV");
                entity.HasOne(d => d.CompteFavori)
                    .WithMany(p => p.FavorisCompte)
                    .HasForeignKey(d => d.CompteId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_FAV_CPT");
                entity.HasOne(d => d.FilmFavori)
                    .WithMany(p => p.FavorisFilm)
                    .HasForeignKey(d => d.FilmId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_FAV_FLM");
            });
        }
    }
}
