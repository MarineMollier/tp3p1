﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using TP3P1.Models.EntityFramework;

namespace TP3P1.Models.EntityFramework
{
    [Table("T_E_COMPTE_CPT")]
    public class Compte
    {
        public Compte()
        {
            FavorisCompte = new HashSet<Favori>();
        }

        //[Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("CPT_ID")]
        public int CompteId { get; set; }
        [Required]
        [Column("CPT_NOM")]
        [StringLength(50)]
        public string Nom { get; set; }
        [Required]
        [Column("CPT_PRENOM")]
        [StringLength(50)]
        public string Prenom { get; set; }
        [Column("CPT_TELPORTABLE", TypeName = "char(10)")] // Impossible de mettre char tout court => char(10)
        [StringLength(10)]
        [RegularExpression(@"^0[0-9]{9}$", ErrorMessage = "Numéro de téléphone invalide.")]
        public string TelPortable { get; set; }
        [Required]
        [Column("CPT_MEL")]
        [EmailAddress]
        [Display(Name = "Email")]
        [StringLength(100, MinimumLength = 6, ErrorMessage = "La longueur d’un email doit être comprise entre 6 et 100 caractères.")]
        public string Mel { get; set; }
        [Column("CPT_PWD")]
        [StringLength(64)]
        public string Pwd { get; set; }
        [Required]
        [Column("CPT_RUE")]
        [StringLength(200)]
        public string Rue { get; set; }
        [Required]
        [Column("CPT_CP", TypeName = "char(5)")] // Impossible de mettre char tout court => char(5)
        [StringLength(5)]
        public string CodePostal { get; set; }
        [Required]
        [Column("CPT_VILLE")]
        [StringLength(50)]
        public string Ville { get; set; }
        [Required]
        [Column("CPT_PAYS")]
        [StringLength(50)]
        public string Pays { get; set; }
        [Column("CPT_LATITUDE")]
        public float? Latitude { get; set; }
        [Column("CPT_LONGITUDE")]
        public float? Longitude { get; set; }
        [Required]
        [Column("CPT_DATECREATION", TypeName = "date")]
        public DateTime DateCreation { get; set; }

        [InverseProperty("CompteFavori")]
        public ICollection<Favori> FavorisCompte { get; set; }

    }
}
