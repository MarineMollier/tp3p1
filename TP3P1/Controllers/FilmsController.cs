﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TP3P1.Models.EntityFramework;
using TP3P1.Models.Repository;

namespace TP3P1.Controllers
{
   
    /// <summary>
    /// Films controller
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class FilmController : ControllerBase
    {
        readonly IDatarepository<Film> _dataRepository;

        /// <summary>
        /// Constructeur pour l'injection de dependance
        /// </summary>
        /// <param name="dataRepository"></param>
        public FilmController(IDatarepository<Film> dataRepository)
        {
            _dataRepository = dataRepository;
        }

        /// <summary>
        /// Retourne la liste de tout les Films en bdd
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("Film")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<Film>>> GetFilm()
        {
            return await _dataRepository.GetAll();
        }

        /// <summary>
        /// retourne le Film correspondant à l'id en parametre
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("ById/{id}")]
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<Film>> GetFilmById(int id)
        {
            var Film = await _dataRepository.GetById(id);

            if (Film.Value == null)
            {
                return NotFound();
            }

            return Film;
        }

        /// <summary>
        /// retorune le Film correspondant au mail en parametre
        /// </summary>
        /// <param name="titre"></param>
        /// <returns></returns>
        [Route("ByEmail/{email}")]
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<Film>> GetFilmByTitre(string titre)
        {
            var Film = await _dataRepository.GetByString(titre);

            if (Film.Value == null)
            {
                return NotFound();
            }

            return Film;
        }

        /// <summary>
        /// Met à jour le Film correspondant à l'id avec les info passé dans l'object Film en parametre
        /// </summary>
        /// <param name="id"></param>
        /// <param name="Film"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFilm(int id, Film Film)
        {
            if (id != Film.FilmId)
            {
                return BadRequest();
            }
            var FilmToUpdate = await _dataRepository.GetById(id);
            if (FilmToUpdate == null)
            {
                return NotFound();
            }
            await _dataRepository.Update(FilmToUpdate.Value, Film);
            return NoContent();
        }

        /// <summary>
        /// Insere l'objet Film en parametre dans la bdd
        /// </summary>
        /// <param name="Film"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<Film>> PostFilm(Film Film)
        {
            await _dataRepository.Add(Film);

            return CreatedAtAction("GetFilm", new { id = Film.FilmId }, Film);
        }

    }
}
