﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TP3P1.Models.EntityFramework;
using TP3P1.Models.Repository;

namespace TP3P1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompteController : ControllerBase
    {
        private readonly FilmRatingsDBContext _context;
        readonly IDatarepository<Compte> _dataRepository;

        /// <summary>
        /// Constructeur pour l'injection de dependance
        /// </summary>
        /// <param name="dataRepository"></param>
        public CompteController(IDatarepository<Compte> dataRepository)
        {
            _dataRepository = dataRepository;
        }

        /// <summary>
        /// Constructeur pour l'injection de dependance
        /// </summary>
        /// <param name="dataRepository"></param>
        /// <param name="context"></param>
        public CompteController(FilmRatingsDBContext context, IDatarepository<Compte> dataRepository)
        {
            _context = context;
            _dataRepository = dataRepository;
        }

        // GET: api/Compte
        [HttpGet]
        [Route("Compte")]
        public async Task<ActionResult<IEnumerable<Compte>>> GetCompte()
        {
            return await _context.Compte.ToListAsync();
        }


        /// <summary>
        /// Retourne la liste de tout les comptes en bdd
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("AllCompte")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<Compte>>> GetAllCompte()
        {
            return await _dataRepository.GetAll();
        }

        /// <summary>
        /// retourne le compte correspondant à l'id en parametre
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("ById/{id}")]
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<Compte>> GetCompteById(int id)
        {
            var compte = await _dataRepository.GetById(id);

            if (compte.Value == null)
            {
                return NotFound();
            }

            return compte;
        }

        /// <summary>
        /// retourne le compte correspondant à l'adresse mail en parametre
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        [Route("ByEmail/{email}")]
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<Compte>> GetCompteByEmail(string email)
        {
            var compte = await _dataRepository.GetByString(email);

            if (compte.Value == null)
            {
                return NotFound();
            }

            return compte;
        }

        /// <summary>
        /// Met à jour le compte correspondant à l'id avec les info passé dans l'object compte en parametre
        /// </summary>
        /// <param name="id"></param>
        /// <param name="compte"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCompte(int id, Compte compte)
        {
            if (id != compte.CompteId)
            {
                return BadRequest();
            }
            var compteToUpdate = await _dataRepository.GetById(id);
            if (compteToUpdate == null)
            {
                return NotFound();
            }
            await _dataRepository.Update(compteToUpdate.Value, compte);
            return NoContent();
        }

        /// <summary>
        /// Insere l'objet compte en parametre dans la bdd
        /// </summary>
        /// <param name="compte"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<Compte>> PostCompte(Compte compte)
        {
            await _dataRepository.Add(compte);

            return CreatedAtAction("GetCompte", new { id = compte.CompteId }, compte);
        }

    }
}
